import os
import webbrowser
from jinja2 import Environment, FileSystemLoader

my_selection = os.listdir("favorites") 
#['aPm7ZQSk.png', '3UrdWXJm.png', 's7LRNXM0.png', 'zfGXwZCw.png', 'c3IQzVfh.jpeg', 'MfdnK8bL.jpg', 'KRhWzeSi.jpg', 'CTO7Wxlm.jpg', 'nlvekaWk.jpg', 'jilDQG0D.jpeg', 'yRx4xu48.jpg', 'VCpTiPzx.jpg', 'Rr754fkg.jpg', 'ncng9wfq.jpg', 'FGjlxoOg.jpg', 'JkBu3DsT.jpg', 'I60qikOF.jpg', 'dYT7HgJf.jpg', 'WfCiKr0Y.jpg', 'YZ3ihCQU.jpg', 'V6wcfutK.jpg', 'OBW7wuWL.png', 'K4n4g4w4.jpg', 'Kp4TPfNg.jpg']


# Define the folder path and base URL
folder_path = "thumbnails"
base_url = f"file://{os.getenv('HOME')}/.local/share/wallpapers/"

# Define the template file
template_file = "scripts/template.html"

# Define the output file
output_file = "index.html"

# Define the number of columns
num_cols = 3

# Load the template file
env = Environment(loader=FileSystemLoader("."))
template = env.get_template(template_file)

# Get a list of image files in the folder
image_files = [f for f in os.listdir(folder_path)]


# Render the template with the images HTML and other variables
output = template.render(
    images=[i for i in image_files if i not in my_selection],
    selection=[i for i in image_files if i in my_selection],
    base_url=base_url,
    num_cols=num_cols
)

# Write the output HTML to a file
with open(output_file, "w") as f:
    f.write(output)
