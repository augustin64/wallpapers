#!/bin/bash

# Specify the input and output directories
input_dir="wallpapers/"
output_dir="thumbnails/"

mkdir -p "$output_dir"

# Loop through each image in the input directory
for file in "$input_dir"*
do
  # Get the file name and extension
  filename=$(basename -- "$file")
  extension="${filename##*.}"

  echo "$filename"
  # Create a downscaled version of the image and save it in the output directory
  convert "$file" -resize "768x>" "$output_dir/$filename"
done
